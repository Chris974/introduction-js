/*Exercice 10 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau ainsi que les clés associés. Cela pourra être, par exemple, de la forme : "Le département" + nom_departement + "a le numéro" + num_departement*/


let departement = new Object();
departement[02]="Aisne";
departement[59]="Nord";
departement[60]="Oise";
departement[62]="Pas-de-Calais";
departement[80]="Somme";

for (let i in departement)
{
console.log("Le département " + departement[i] + " a le numéro " + i);
}

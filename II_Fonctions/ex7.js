/*Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

    Vous êtes un homme et vous êtes majeur
    Vous êtes un homme et vous êtes mineur
    Vous êtes une femme et vous êtes majeur
    Vous êtes une femme et vous êtes mineur*/


  function you(age, genre)
  {
    if (genre="homme")
    {
      return "Vous êtes un homme";
    }
    else
    {
      return "Vous êtes une femme";
    }
    if (age>=18)
    {
      return "Vous êtes majeur";
    }
    else
    {
      return "Vous êtes mineur";
    }
  }
  you(17, "homme");
